// run `gcc example.c; ./a.out`

#include <stdio.h>
#include "n_ranges.h"

int main() {
	struct n_ranges_Desc desc = n_ranges_desc(27, 4);
	for (int i=0; i<4; ++i) {
		struct n_ranges_Range r = n_ranges_range_at_index(27, 4, i, &desc);
		printf("(%d, %d)\n", r.begin, r.end);
	}
	return 0;
}
