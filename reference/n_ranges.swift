import Foundation

func _gcd(a: Int, b: Int) -> Int {
    if b == 0 {
        return a
    } else {
    	return _gcd(a: b, b: a % b)
    }
}


func _positiveRatio(n: Int, d: Int) -> (numerator: Int, denominator: Int) {
	precondition(n > 0 || d > 0)
	let gcd = _gcd(a: n, b: d)
	return (numerator: n / gcd, denominator: d / gcd)
}

public struct NRangesDesc {
	init(_ numElements: Int, _ numRanges: Int) {
		self.numElements = numElements
		self.numRanges = numRanges
		self.elementsPerRange = numElements / numRanges
		self.ratio = _positiveRatio(n: numElements % numRanges, 
							  		d: numRanges)
	}
	let numElements: Int
	let numRanges: Int
	let elementsPerRange: Int
	let ratio: (numerator: Int, denominator: Int)
}


func _rangeIndexToElementIndex(_ desc: NRangesDesc, _ rangeIndex: Int) -> Int {
	precondition(rangeIndex <= desc.numRanges)
	if desc.ratio.numerator == 0 {
		return rangeIndex * desc.elementsPerRange
	} else if rangeIndex == desc.numRanges {
		return desc.numElements
	} else {
		let extra = (rangeIndex * desc.ratio.numerator 
					% desc.ratio.denominator < desc.ratio.numerator) ? 0 : 1;
		return rangeIndex * desc.elementsPerRange 						 
			+ rangeIndex * desc.ratio.numerator / desc.ratio.denominator
			+ extra
	}
}


public func rangeIndexToElementIndex(numElements: Int, numRanges: Int, 
							  		 rangeIndex: Int, desc: NRangesDesc?) -> Int {
	return _rangeIndexToElementIndex(desc ?? NRangesDesc(numElements, numRanges),
									 rangeIndex);
}


public func rangeAtIndex(numElements: Int, numRanges: Int, rangeIndex: Int, 
				  		 desc: NRangesDesc?) -> (begin: Int, end: Int) {
	let desc = desc ?? NRangesDesc(numElements, numRanges)
	return (begin: _rangeIndexToElementIndex(desc, rangeIndex),
			  end: _rangeIndexToElementIndex(desc, rangeIndex + 1));
}

struct NRanges {
	var rangeBegin: Int
	var rangeEnd: Int
	var desc: NRangesDesc

	init(numElements: Int, numRanges: Int, 
		 rangeBegin: Int = 0, rangeEnd: Int = -1) {
		self.rangeBegin = rangeBegin
		self.rangeEnd = rangeEnd
		if self.rangeEnd < 0 {
			self.rangeEnd = numRanges
		}
		self.desc = NRangesDesc(numElements, numRanges)
	}
}

struct NRangesIterator: IteratorProtocol {
	var ranges: NRanges
	var rangeBegin: Int
	var begin: Int
	var end: Int = 0

	init(ranges: NRanges) {
		self.ranges = ranges
		self.rangeBegin = self.ranges.rangeBegin
		self.begin = _rangeIndexToElementIndex(self.ranges.desc, self.rangeBegin)
	}

	mutating func next() -> (begin: Int, end: Int)? {
		guard self.rangeBegin < self.ranges.rangeEnd else { return nil }
		self.end = _rangeIndexToElementIndex(self.ranges.desc, self.rangeBegin + 1)
		let range = (begin: self.begin, end: self.end)
		swap(&self.begin, &self.end)
		self.rangeBegin += 1
		return range
	}
}

extension NRanges: Sequence {
	func makeIterator() -> NRangesIterator {
		return NRangesIterator(ranges: self)
	}
}
