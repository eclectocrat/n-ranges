var assert = require('assert');

function _gcd(a, b) {
    if (!b) {
        return a;
    } else {
    	return _gcd(b, a % b);
    }
}


function _positive_ratio(n, d) {
	assert(n > 0 || d > 0);
	const gcd = _gcd(n, d)
	return {
		numerator: Math.trunc(n / gcd),
		denominator: Math.trunc(d / gcd)
	};
}


function _desc(num_elements, num_ranges) {
	return {
		num_elements: num_elements, 
		num_ranges: num_ranges, 
		elements_per_range: Math.trunc(num_elements / num_ranges), 
		ratio: _positive_ratio(num_elements % num_ranges, num_ranges) 
	};
}


function _range_index_to_element_index(desc, range_index) {
	assert(range_index <= desc.num_ranges)
	if (desc.ratio.numerator == 0) {
		return range_index * desc.elements_per_range;
	} else if (range_index == desc.num_ranges) {
		return desc.num_elements;
	} else {
		const extra = (range_index * desc.ratio.numerator 
					% desc.ratio.denominator < desc.ratio.numerator) ? 0 : 1;
		return range_index * desc.elements_per_range 						 
			+ Math.trunc(range_index * desc.ratio.numerator / desc.ratio.denominator)
			+ extra;
	}
}


function range_index_to_element_index(num_elements, num_ranges, range_index, 
									  desc) {
	return _range_index_to_element_index(desc || _desc(num_elements, 
													   num_ranges), 
										range_index);
}


function range_at_index(num_elements, num_ranges, range_index, desc) {
	const desc = desc || _desc(num_elements, num_ranges);
	return {begin: _range_index_to_element_index(_desc, range_index),
			  end: _range_index_to_element_index(_desc, range_index + 1)};
}


function* n_ranges(num_elements, num_ranges, range_begin=0, range_end=-1, desc) {
	if (range_end < 0) {
		range_end = num_ranges;
	}
	desc = desc || _desc(num_elements, num_ranges);
	let begin = _range_index_to_element_index(desc, range_begin);
	while (range_begin < range_end) {
		let end = _range_index_to_element_index(desc, range_begin + 1);
		yield ({begin: begin, end: end});
		[begin, end] = [end, begin];
		range_begin += 1;
	}
}

module.exports = n_ranges