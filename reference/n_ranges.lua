
function _gcd(a, b)
    if b == 0 
    	then return a
 		else return _gcd(b, a % b)
    end
end

function _positive_ratio(n, d)
	assert(n > 0 or d > 0)
	local gcd = _gcd(n, d)
	return {numerator = n // gcd, denominator = d // gcd}
end

function _desc(num_elements, num_ranges)
	return {
		num_elements = num_elements, 
		num_ranges = num_ranges, 
		elements_per_range = num_elements // num_ranges, 
		ratio = _positive_ratio(num_elements % num_ranges, num_ranges) 
	}
end

function _range_index_to_element_index(desc, range_index)
	assert(range_index <= desc.num_ranges)
	if desc.ratio.numerator == 0 then
		return range_index * desc.elements_per_range
	elseif range_index == desc.num_ranges then
		return desc.num_elements
	else
		local extra =(range_index * desc.ratio.numerator 					 
					 % desc.ratio.denominator < desc.ratio.numerator) and 0 or 1
		return range_index * desc.elements_per_range 						
			+ (range_index * desc.ratio.numerator // desc.ratio.denominator)
			+ extra
	end
end

function range_index_to_element_index(num_elements, num_ranges, range_index, desc)
	return _range_index_to_element_index(
		desc or _desc(num_elements, num_ranges), range_index)
end


function range_at_index(num_elements, num_ranges, range_index, desc)
	desc = desc or _desc(num_elements, num_ranges)
	return {_range_index_to_element_index(_desc, range_index), 
			_range_index_to_element_index(_desc, range_index + 1)}
end

function n_ranges(num_elements, num_ranges, range_begin, range_end, desc)
	if not range_begin then range_begin = 0 end
	if not range_end then range_end = num_ranges end
	desc = desc or _desc(num_elements, num_ranges)
	local begin_index, end_index = _range_index_to_element_index(desc, range_begin), nil
	return function () 
		if range_begin < range_end then
			end_index = _range_index_to_element_index(desc, range_begin + 1)
			local result = {begin_index, end_index}
			begin_index, end_index = end_index, begin_index
			range_begin = range_begin + 1
			return result
		end
	end
end
