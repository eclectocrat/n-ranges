#ifndef _n_ranges_h
#define _n_ranges_h

#include <assert.h>

int _n_ranges_gcd (int a, int b) {
    if (!b) {
        return a;
    } else {
    	return _n_ranges_gcd(b, a % b);
    }
}

struct n_ranges_Ratio {
	int numerator, denominator;
};

struct n_ranges_Ratio _n_ranges_positive_ratio(int n, int d) {
	assert(n > 0 || d > 0);
	const int gcd = _n_ranges_gcd(n, d);
	return (struct n_ranges_Ratio){ n / gcd, d / gcd };
}

struct n_ranges_Desc {
	int num_elements, num_ranges, elements_per_range;
	struct n_ranges_Ratio ratio;
};

struct n_ranges_Desc n_ranges_desc(int num_elements, int num_ranges) {
	return (struct n_ranges_Desc) { 
		num_elements, num_ranges, num_elements / num_ranges, 
		_n_ranges_positive_ratio(num_elements % num_ranges, num_ranges) };
}

int _n_ranges_range_index_to_element_index(struct n_ranges_Desc * desc, int range_index) {
	assert(range_index <= desc->num_ranges);
	if (desc->ratio.numerator == 0) {
		return range_index * desc->elements_per_range;
	} else if (range_index == desc->num_ranges) {
		return desc->num_elements;
	} else {
		const int extra = (range_index * desc->ratio.numerator 
						% desc->ratio.denominator < desc->ratio.numerator) ? 0 : 1;
		return range_index * desc->elements_per_range 						 
			+ range_index * desc->ratio.numerator / desc->ratio.denominator
			+ extra;
	}
}

struct n_ranges_Range {
	int begin, end;
};

int n_ranges_range_index_to_element_index(int num_elements, int num_ranges, int range_index, 
										  struct n_ranges_Desc * desc) {
	struct n_ranges_Desc _desc;
	if (!desc) {
		_desc = n_ranges_desc(num_elements, num_ranges);	
		desc = &_desc;
	} 
	return _n_ranges_range_index_to_element_index(desc, range_index);
}

struct n_ranges_Range n_ranges_range_at_index(int num_elements, int num_ranges, int range_index, 
											  struct n_ranges_Desc * desc) {
	struct n_ranges_Desc _desc;
	if (!desc) {
		_desc = n_ranges_desc(num_elements, num_ranges);	
		desc = &_desc;
	}
	return (struct n_ranges_Range) { _n_ranges_range_index_to_element_index(desc, range_index),
				  		 			 _n_ranges_range_index_to_element_index(desc, range_index + 1) };
}

#endif