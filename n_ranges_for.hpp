#ifndef ec_n_ranges_for_hpp
#define ec_n_ranges_for_hpp

#include "n_ranges.hpp"

namespace ec {
/**
    Visits a range of ranges of the given N_ranges, where elements > ranges.

    Given a visitor function object, calls it for each range of ranges given,
    passing an Uint_t index of the range, and a Range<Uint_t> of the element
    indexes which make up that range.

    For example, to display the minimum and maximum elements of a data set:
    @code{.cpp}
        const size_t data_size = ...;
        vector<float> data(data_size);
        // Show the second quarter of data.
        Range<size_t> window(data_size/4, data_size/2);
        N_ranges<size_t> ranges(data_size, window.size());
        n_ranges_compress_visit(ranges, window,
            [&](auto index, auto range) {
                auto [min, max] = minmax_element(data);
                render_peak(index, *min, *max);
            });
    @endcode

    @param ranges           An N_ranges instance to use.
    @param range_of_ranges  The range of ranges to visit. Must fit within the
                            given N_ranges bounds.
    @param visitor          A function object that can be invoked with a Uint_t
                            index and a Range<Uint_t> of element indexes.
                            Lambda's with auto-deduced parameters of the form
                            [](auto index, auto range) { ... } work well.
 */
template <typename Uint_t, typename N, typename F,
          typename = std::enable_if<std::is_integral<N>::value>>
constexpr void for_n_ranges_compress(N_ranges<Uint_t> ranges,
                                     Range<N> range_of_ranges, F visitor) {
    expects(ranges.elements_size() > ranges.ranges_size());
    for (Uint_t i = from(range_of_ranges), b = ranges.location_of_range(i);
         i < to(range_of_ranges); ++i) {
        Uint_t e = ranges.location_of_range(i+1);
        visitor(i, Range<Uint_t>(b, e));
        std::swap(b, e);
    }
}


template <typename Uint_t, typename N, typename F,
          typename = std::enable_if<std::is_integral<N>::value>>
constexpr void for_n_ranges_expand(N_ranges<Uint_t> const &ranges,
                                   Range<N> range_of_ranges, F visitor) {
    expects(ranges.elements_size() < ranges.ranges_size());
    for (Uint_t i = from(range_of_ranges); i < to(range_of_ranges); ++i) {
        visitor(i, Range<Uint_t>(ranges.location_of_range(i),
                                 ranges.location_of_range(i + 1)));
    }
}


template <typename Uint_t, typename N, typename F,
          typename = std::enable_if<std::is_integral<N>::value>>
constexpr void for_n_ranges_identity(N_ranges<Uint_t> ranges,
                                     Range<N> range_of_ranges, F visitor) {
    expects(ranges.elements_size() == ranges.ranges_size());
    for(size_t i = 0; i<ranges.elements_size(); ++i) {
        visitor(i, Range<Uint_t>(i, i + 1));
    }
}


template <typename Uint_t, typename N, typename F,
          typename = std::enable_if<std::is_integral<N>::value>>
constexpr void for_n_ranges(N_ranges<Uint_t> const &ranges,
                            Range<N> range_of_ranges, F visitor) {
    if (ranges.elements_size() < ranges.ranges_size()) {
        for_n_ranges_expand(ranges, range_of_ranges, visitor);
    } else if (ranges.elements_size() == ranges.ranges_size()) {
        for_n_ranges_identity(ranges, range_of_ranges, visitor);
    } else { // ranges.elements_size() > ranges.ranges_size()
        for_n_ranges_compress(ranges, range_of_ranges, visitor);
    }
}


template <typename Uint_t, typename F>
constexpr void for_all_n_ranges(N_ranges<Uint_t> const &ranges, F visitor) {
    for_n_ranges(ranges, Range<Uint_t>(ranges.ranges_size()), visitor);
}

}
#endif
