#ifndef _ec_n_ranges_hpp
#define _ec_n_ranges_hpp

#include <expects/expects.hpp>
#include <utility>
#include <type_traits>

// Define EC_N_RANGES_RANGE before including this file to provide a custom
// range type. You must also provide `from` and `to` functions.
#ifndef EC_N_RANGES_RANGE
#include "range.hpp"
#define EC_N_RANGES_RANGE Range
#endif

namespace ec {
namespace detail {
template <typename T> constexpr T greatest_common_divisor(T n, T d) {
    return d == 0 ? n : greatest_common_divisor<T>(d, n % d);
}

template <typename T>
constexpr std::pair<T, T> positive_ratio(T num, T den) TESTEXCEPT {
    static_assert(std::is_unsigned<T>::value);
    expects(num > 0 or den > 0);
    const T gcd = greatest_common_divisor(num, den);
    const T result_num = num / gcd;
    const T result_den = den / gcd;
    return std::make_pair(result_num, result_den);
};
} // namespace detail


/// \brief  Transforms range index into an element index.
/// \see    n_ranges_location_of_range() for how to pre-calculate parameters.
///
/// \param input_size   Number of elements to split into ranges.
/// \param output_size  Number of ranges to group the input elements into.
/// \param inputs_per_output    Pre-calculated number of elements per range,
///                             rounded down.
/// \param remainder_ratio      Pre-calculated ratio of elements per range.
/// \param n                    Range index to transform into an element index.
///
template <typename Uint_t>
constexpr Uint_t n_ranges_transform_location(
    const Expects_nonzero<Uint_t> input_size,
    const Expects_nonzero<Uint_t> output_size,
    const Uint_t inputs_per_output,
    const std::pair<Uint_t, Expects_nonzero<Uint_t>> remainder_ratio,
    const Uint_t n) TESTEXCEPT {

    //static_assert(std::is_unsigned<Uint_t>::value);
    expects(n <= output_size);
    
    if (remainder_ratio.first == 0) {
        return n * inputs_per_output;
    } else if (n == output_size) {
        return input_size;
    } else {
        return n * inputs_per_output +
               (n * remainder_ratio.first / remainder_ratio.second) +
               ((n * remainder_ratio.first % remainder_ratio.second <
                 remainder_ratio.first)
                    ? 0
                    : 1);
    }
}


/// \brief  Transforms a range index into an element index.
/// \see    n_ranges_transform_location() for a function that takes in pre-
///         calculated values, for better performance for repeated calls.
///
/// \param input_size   Number of elements to evenly split into ranges.
/// \param output_size  Number of ranges to split elements into.
/// \param n            Range index (<= output_size), to transform into an
///                     element index (<= input_size).
///
template <typename Uint_t>
constexpr Uint_t
n_ranges_location_of_range(const Expects_nonzero<Uint_t> input_size,
                           const Expects_nonzero<Uint_t> output_size,
                           const Uint_t n) TESTEXCEPT {
    expects(n <= output_size);
    const Uint_t inputs_per_output = input_size / output_size;
    const auto remainder_ratio =
        detail::positive_ratio(input_size % output_size, Uint_t(output_size));
    return n_ranges_transform_location<Uint_t>(
        input_size, output_size, inputs_per_output, remainder_ratio, n);
}


template <typename Uint_t>
class N_ranges {
public:
    constexpr N_ranges() = default;
    constexpr N_ranges(N_ranges const&) = default;
    N_ranges& operator = (N_ranges const&) = default;

    constexpr N_ranges(Expects_nonzero<Uint_t> inp,
                       Expects_nonzero<Uint_t> outp) TESTEXCEPT
        : _input_size(inp), _output_size(outp), _inputs_per_output(inp / outp),
          _remainder_ratio(
              detail::positive_ratio<Uint_t>(inp % outp, outp)) {
        expects(_remainder_ratio.first < _output_size);
    }

    constexpr Uint_t location_of_range(Uint_t n) const TESTEXCEPT {
        return n_ranges_transform_location<Uint_t>(
            _input_size, _output_size, _inputs_per_output, _remainder_ratio, n);
    }
    
    constexpr Range<Uint_t> range_at_location(Uint_t n) const TESTEXCEPT {
        return Range<Uint_t>(location_of_range(n), location_of_range(n + 1));
    }

    constexpr Uint_t elements_size() const noexcept { return _input_size; }
    constexpr Uint_t ranges_size() const noexcept { return _output_size; }
    constexpr Uint_t elements_per_range() const noexcept {
        return _inputs_per_output;
    }
    constexpr Uint_t range_min_size() const noexcept {
        return _inputs_per_output;
    }
    constexpr Uint_t range_max_size() const noexcept {
        return _inputs_per_output + (_remainder_ratio.first > 0 ? 1 : 0);
    }

private:
    Uint_t _input_size = 0;
    Uint_t _output_size = 0;
    Uint_t _inputs_per_output = 0;
    std::pair<Uint_t, Uint_t> _remainder_ratio = {0, 0};
};

}
#endif
