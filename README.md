# n-ranges
A library to split a discrete amount of elements into N discrete ranges, with division remainder being smeared linearly. Includes a slightly embelished C++17 library and minimal reference implementations in several languages (C, Python, Lua, Haskell, JavaScript).

Copyright(C) Jeremy Jurksztowicz 2020. All rights reserved.

### Concepts

Starting with a range of length M:

![base range](doc/algorithm-step1.png)

Dividing it into N+1 discrete bins, where the last bin has M % N elements:

![base range](doc/algorithm-step2.png)

Using n_ranges, divide it up while linearly smearing the remainder across the entire range and preserving order:

![base range](doc/algorithm-step3.png)

### Example Use Cases

- Bin samples across N horizontal pixels for rendering. (Motivating example)
- Bin tasks for execution across N cores.
- Remap discrete arrays to new sizes.

### Implementation

Naive, untested, and certainly sub-optimal, but fast enough to do realtime audio signal rendering on a computer or phone, and realtime continuous array transformations on an ARM chip running at 72Mhz. Ranges are calculated lazily, but there are a lot of modulo operations, so be aware of your processor's performance characteristics. For many chips using a small integer type like `unsigned char` results in much faster modulo operations, so you might want to experiment with that.
