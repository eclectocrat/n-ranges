#ifndef _ec_range_hpp
#define _ec_range_hpp

#include <expects/expects.hpp>
#include <type_traits>
#include <utility>

namespace ec {

template <typename I=size_t> struct Range {
    typedef I Index;

    constexpr Range() noexcept = default;
    constexpr Range(Index f, Index t) TESTEXCEPT : _from(f), _to(t) {
        expects(_from <= _to);
    }
    constexpr Range(Index t) TESTEXCEPT : _from(0), _to(t) {
        expects(_from <= _to);
    }
    constexpr Range(std::pair<Index, Index> const &p) noexcept
        : Range(p.first, p.second) {}
    constexpr Index from() const noexcept { return _from; }
    constexpr Index to() const noexcept { return _to; }
    constexpr bool empty() const noexcept { return from() == to(); }
    constexpr Index size() const noexcept { return to() - from(); }

    constexpr Range slice(const Index f, const Index t) const {
        expects(f <= size());
        expects(t <= size());
        expects(t - f <= size());
        expects(f <= t);
        return Range(from() + f, from() + t);
    }

    constexpr bool contains(Index i) const noexcept {
        return from() <= i and to() >= i;
    }
    constexpr bool contains(Range const &other) const noexcept {
        return from() <= other.from() and to() >= other.to();
    }
    constexpr bool is_before(Range const &other) const noexcept {
        return to() == other.from();
    }
    constexpr bool is_after(Range const &other) const noexcept {
        return from() == other.to();
    }

private:
    Index _from = Index{};
    Index _to = Index{};
};

template <typename Index>
constexpr bool operator<(Range<Index> const &a,
                         Range<Index> const &b) noexcept {
    return a.from() < b.from();
}

template <typename Index>
constexpr bool operator==(Range<Index> const &a,
                          Range<Index> const &b) noexcept {
    return a.from() == b.from() and a.to() == b.to();
}

template <typename Index>
constexpr Range<Index> make_range(Index f, Index t) TESTEXCEPT {
    return Range<Index>(f, t);
}

template <typename I> constexpr I from(Range<I> const &r) noexcept {
    return r.from();
}
template <typename I> constexpr I to(Range<I> const &r) noexcept {
    return r.to();
}
template <typename I> constexpr bool empty(Range<I> const &r) noexcept {
    return r.empty();
}
template <typename I> constexpr I size(Range<I> const &r) noexcept {
    return r.size();
}

template<typename I>
constexpr Range<I> offset(Range<I> r, int amt) {
    return Range<I>(from(r)+amt, to(r)+amt);
}

template<typename I>
constexpr Range<I> intersection(Range<I> a, Range<I> b) {
    if (to(a) <= from(b) or to(b) <= from(a)) {
        return Range<I>();
    } else {
        return Range<I>(std::max(from(a), from(b)), std::min(to(a), to(b)));
    }
}

/// Returns the pair of ranges made by splitting the given range at the given
/// offset from `from()`.
///
/// \param r        Range to split.
/// \param index    Index at which to split this range: 0 <= index <= to.
/// \return A pair of ranges, the `first` being the range left of the split
///         index (from->index), and the `second` being the range right of the
///         split index (index->to).
template<typename I>
constexpr auto split_range(Range<I> const& r, const I index) {
    expects(index <= size(r));
    return std::make_pair(Range<I>(from(r), from(r) + index),
                          Range<I>(from(r) + index, to(r)));
}

}
#endif
